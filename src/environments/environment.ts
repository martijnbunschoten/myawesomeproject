// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBWDlV_3CmkQBh87Zswu3L5i-yYoBxby0U",
    authDomain: "todolistapp-4c56a.firebaseapp.com",
    databaseURL: "https://todolistapp-4c56a.firebaseio.com",
    projectId: "todolistapp-4c56a",
    storageBucket: "",
    messagingSenderId: "817903682782",
    appId: "1:817903682782:web:c1984dc59dd4ce5e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
